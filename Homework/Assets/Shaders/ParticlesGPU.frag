#version 430

uniform sampler2D uTex;
uniform vec3 uRandomColorSeed;

in vec2 texcoord;
in float frag_alpha;

out vec4 FragColor;

void main()
{
	FragColor = vec4(uRandomColorSeed.x, uRandomColorSeed.y, uRandomColorSeed.z, 1) * texture(uTex, texcoord).rgba * frag_alpha;
}