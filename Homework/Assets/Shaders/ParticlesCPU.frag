#version 430

uniform sampler2D uTex;
uniform vec3 uRandomColorSeed;

in vec2 texcoord;
in float alpha;

out vec4 FragColor;

vec3 newSeed;

// Returns random number from zero to one
float randZeroOne(vec3 seed)
{
	uint n = floatBitsToUint(seed.x * 2531011.0 + seed.y * 214013.0 + seed.z * 141251.0);
	n = n * (n * n * 15731u + 789221u);
	n = (n >> 9u) | 0x3F800000u;

	float result = 2.0 - uintBitsToFloat(n);

	newSeed = vec3(seed.x + 147158.0 * result, seed.y * result + 415161.0 * result, seed.z + 324154.0 * result);

	return result;
}

void main()
{
	newSeed = uRandomColorSeed;
	FragColor = vec4(randZeroOne(newSeed), randZeroOne(newSeed), randZeroOne(newSeed), 1) * texture(uTex, texcoord).rgba * alpha;
}