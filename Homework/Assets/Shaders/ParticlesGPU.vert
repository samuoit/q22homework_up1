#version 430

// VBO data received from C++
layout(location = 0) in vec3 posIn;
layout(location = 1) in float sizeIn;
layout(location = 2) in float alphaIn;

// Uniform constants
uniform mat4 uModel;
uniform mat4 uView;

// Vertex shader output
out float size;
out float alpha;

void main()
{
	gl_Position = uView * uModel * vec4(posIn, 1.0);	// Vertex position
	size = sizeIn;										// Particle size
	alpha = alphaIn;									// Alpha blending factor
}