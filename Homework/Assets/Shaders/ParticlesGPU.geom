#version 430

// Geometry 'in' and 'out'
layout (points) in;									// points comming in
layout (triangle_strip, max_vertices = 4) out;		// triangle strips comming out

// Inputs
in float size[];
in float alpha[];

// Additional outputs
out vec2 texcoord;
out float frag_alpha;

// Uniforms
uniform mat4 uProj;

vec4 vertPos;

// Output vertex (to fragment shader) specified by position offset, texture, and coordinates
void OutputPoint(vec2 positionOffset, vec2 txCoord, float alphaFactor)
{
	gl_Position = uProj * vec4(vertPos.x + positionOffset.x, vertPos.y + positionOffset.y, vertPos.z, vertPos.w);
	texcoord = txCoord;
	frag_alpha = alphaFactor;
	EmitVertex();
}

void main()
{
	// Get position passed by vertex shader
	vertPos = gl_in[0].gl_Position;

	// Set texture coordinates for different corners
	vec2 leftTopCoord = vec2(0.0, 0.0);
	vec2 leftBottomCoord = vec2(0.0, 1.0);
	vec2 rightBottomCoord = vec2(1.0, 0.0);
	vec2 rightTopCoord = vec2(1.0, 1.0);

	// Emit (output) vertex with position, texture, and alpha setting
	OutputPoint(vec2(-0.5, -0.5) * size[0], leftTopCoord, alpha[0]);
	OutputPoint(vec2(0.5, -0.5) * size[0], leftBottomCoord, alpha[0]);
	OutputPoint(vec2(-0.5, 0.5) * size[0], rightBottomCoord, alpha[0]);
	OutputPoint(vec2(0.5, 0.5) * size[0], rightTopCoord, alpha[0]);
	
	// End primitive - We just need 4 corners for quad
	EndPrimitive();
}