#include "Game.h"
#include "Utilities.h"

Game::Game() {}

Game::~Game()
{
	delete updateTimer;
	ParticleProgram.UnLoad();
}

void Game::initializeGame()
{
	updateTimer = new Timer();

	InitFullScreenQuad();

	glEnable(GL_DEPTH_TEST);
		
	// Particle system initialization
	if (!particleSystem1.Init(
		vec3(-15.0f, -10.0, -15.0f),			// Minimum position of particles
		vec3(15.0f, 10.0f, -2.0f),				// Maximum position of particles
		vec3(-5.0f, -5.0f, -5.0f),				// Miminum velocity of particles
		vec3(5.0f, 5.0f, 5.0f),					// Maximum velocity of particles
		vec2(4.0f, 15.0f),						// Life time range
		vec2(0.6f, 1.0f),						// Alpha blending range
		vec2(0.5f, 2.0f),						// Size range
		200,									// Maximum number of particles spawned at a time
		10,										// Rate of particles spawned
		"./Assets/Textures/particle_snow1.png",	// Custom texture file path if particles used are PARTICLE_CUSTOM
		false									// false = Use C++ generated particles, true = Use geometry shader to generate particles from particle centers via geometry shader
		))
	{
		std::cout << "Particle system 1 failed to initialize.\n";
		system("pause");
		exit(0);
	}

	// Particle system 2 initialization
	if (!particleSystem2.Init(
		vec3(-15.0f, -10.0, -15.0f),			// Minimum position of particles
		vec3(15.0f, 10.0f, -2.0f),				// Maximum position of particles
		vec3(-5.0f, -5.0f, -5.0f),				// Miminum velocity of particles
		vec3(5.0f, 5.0f, 5.0f),					// Maximum velocity of particles
		vec2(4.0f, 15.0f),						// Life time range
		vec2(0.6f, 1.0f),						// Alpha blending range
		vec2(0.1f, 0.4f),						// Size range
		2000,									// Maximum number of particles spawned at a time
		100,									// Rate of particles spawned
		"./Assets/Textures/particle_fire1.png",	// Custom texture file path if particles used are PARTICLE_CUSTOM
		false									// false = Use C++ generated particles, true = Use geometry shader to generate particles from particle centers via geometry shader
		))
	{
		std::cout << "Particle system 2 failed to initialize.\n";
		system("pause");
		exit(0);
	}

	// Particle system 3 initialization
	if (!particleSystem3.Init(
		vec3(-1.5f, -2.0, -1.5f),				// Minimum position of particles
		vec3(2.5f, 0.0f, -2.5f),				// Maximum position of particles
		vec3(-0.25f, -0.3f, -0.25f),			// Miminum velocity of particles
		vec3(0.75f, 0.3f, 0.75f),				// Maximum velocity of particles
		vec2(1.0f, 3.0f),						// Life time range
		vec2(0.4f, 0.6f),						// Alpha blending range
		vec2(0.1f, 0.25f),						// Size range
		20000,									// Maximum number of particles spawned at a time
		400,									// Rate of particles spawned
		"./Assets/Textures/particle_fire2.png",	// Custom texture file path if particles used are PARTICLE_CUSTOM
		true									// false = Use C++ generated particles, true = Use geometry shader to generate particles from particle centers via geometry shader
		))
	{
		std::cout << "Particle system 3 failed to initialize.\n";
		system("pause");
		exit(0);
	}

	CameraTransform.Translate(0.0f, 0.0f, 5.0f);
	CameraProjection.FrustumProjection(80.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 1.0f, 10000.0f);

	ParticleSystemTransform1.Translate(vec3(1.0f, 1.0f, -5.0f));
	particleSystem1.SetMatrices(CameraProjection, CameraTransform.GetInverse(), ParticleSystemTransform1);

	ParticleSystemTransform2.Translate(vec3(1.0f, 1.0f, -5.0f));
	particleSystem2.SetMatrices(CameraProjection, CameraTransform.GetInverse(), ParticleSystemTransform2);

	ParticleSystemTransform3.Translate(vec3(2.0f, 2.0f, -4.0f));
	particleSystem3.SetMatrices(CameraProjection, CameraTransform.GetInverse(), ParticleSystemTransform3);
}

void Game::update()
{
	// update our clock so we have the delta time since the last update
	updateTimer->tick();
	float deltaTime = updateTimer->getElapsedTimeSeconds();
	TotalGameTime += deltaTime;

	// update particle system
	particleSystem1.Update(deltaTime);
	particleSystem2.Update(deltaTime);
	particleSystem3.Update(deltaTime);
}

void Game::draw()
{
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw particles
	particleSystem1.Draw();
	particleSystem2.Draw();
	particleSystem3.Draw();

	glutSwapBuffers();
}

void Game::keyboardDown(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::keyboardUp(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 32: // the space bar
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::mouseClicked(int button, int state, int x, int y)
{
	if(state == GLUT_DOWN) 
	{
		switch(button)
		{
		case GLUT_LEFT_BUTTON:

			break;
		case GLUT_RIGHT_BUTTON:
		
			break;
		case GLUT_MIDDLE_BUTTON:

			break;
		}
	}
	else
	{

	}
}

void Game::mouseMoved(int x, int y)
{
}
