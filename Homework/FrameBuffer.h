#pragma once
#include <GL/glew.h>

class FrameBuffer
{
public:
	FrameBuffer() = delete;	// Will not allow use of default constructor
	FrameBuffer(unsigned numColorAttachments);
	~FrameBuffer();

	void InitDepthTexture(unsigned width, unsigned height);
	void InitColorTexture(unsigned index, unsigned width, unsigned height, GLint internalFormat, GLint filter, GLint wrap);
	bool CheckFBO();

	// Clears all OpenGL memory
	void Unload();

	// Clears all attached textures
	void Clear();

	void Bind();
	void UnBind();

	void MoveToBackBuffer(int windowWidth, int windowHeight);

	GLuint GetColorHandle(unsigned index) const;
	GLuint GetDepthHandle() const;

private:
	GLuint _FBO = GL_NONE;
	GLuint _DepthAttachment = GL_NONE;
	GLuint *_ColorAttachments = nullptr;
	GLenum *_Bufs = nullptr;

	unsigned int _NumColorAttachments = 0;

};