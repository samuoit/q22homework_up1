#include "Utilities.h"

#include <GL/glew.h>

#define BUFFER_OFFSET(i) ((char *)0 + (i))

GLuint _fullScreenQuadVAO = GL_NONE;
GLuint _fullScreenQuadVBO = GL_NONE;

void InitFullScreenQuad()
{
	float VBO_DATA[] =
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,

		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,

		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f
	};

	int vertexSize = 6 * 3 * sizeof(float);
	int texCoordSize = 6 * 2 * sizeof(float);

	glGenVertexArrays(1, &_fullScreenQuadVAO);
	glBindVertexArray(_fullScreenQuadVAO);

	glEnableVertexAttribArray(0);	// vertices
	glEnableVertexAttribArray(1);	// UV coordinates

	glGenBuffers(1, &_fullScreenQuadVBO);

	glBindBuffer(GL_ARRAY_BUFFER, _fullScreenQuadVBO);
	glBufferData(GL_ARRAY_BUFFER, vertexSize + texCoordSize, VBO_DATA, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexSize));

	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	glBindVertexArray(GL_NONE);

}

void DrawFullScreenQuad()
{
	glBindVertexArray(_fullScreenQuadVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(GL_NONE);
}