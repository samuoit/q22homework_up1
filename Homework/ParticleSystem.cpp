#include "ParticleSystem.h"

/*
	To draw two triangles next to each other (and create square):
	0 - bottom left, 
	1, 3 - bottom right, 
	2, 4 - top left, 
	5 - top right
*/
vec2 GenerateTextureCoordinates(int triangleCorner)
{
	switch (triangleCorner)
	{
	case 0:	// bottom left
		return vec2(0, 0);
		break;
	case 1:	// bottom rigth
	case 3:
		return vec2(1, 0);	
		break;
	case 2:	// top left
	case 4:
		return vec2(0, 1);
		break;
	case 5:	// top right
		return vec2(1, 1);		
		break;
	};
}

/* 
	To draw two triangles next to each other (and create square):
	0 - bottom left, 
	1, 3 - bottom right, 
	2, 4 - top left, 
	5 - top right
*/
vec4 GenerateParticleTriangleCorner(int corner, vec3 particleCenter, float size)
{
	float halfSize = size * 0.5;
	switch (corner)
	{
	case 0:	// bottom left
		return vec4(particleCenter.x - halfSize, particleCenter.y - halfSize, particleCenter.z, 1.0);
		break;
	case 1:	// bottom right
	case 3:
		return vec4(particleCenter.x + halfSize, particleCenter.y - halfSize, particleCenter.z, 1.0);
		break;
	case 2:	// top left
	case 4:
		return vec4(particleCenter.x - halfSize, particleCenter.y + halfSize, particleCenter.z, 1.0);
		break;
	case 5:	// top right
		return vec4(particleCenter.x + halfSize, particleCenter.y + halfSize, particleCenter.z, 1.0);
		break;
	};
}

float GenerateRandomFloat(float min, float max, unsigned seed = 0)
{
	return min + ((max - min) * rand()) / (RAND_MAX + 1.0f);
}

ParticleSystem::ParticleSystem(){}

ParticleSystem::~ParticleSystem()
{
	// Unload shader program and shaders
	progParticleSystem.UnLoad();
	
	// Cleanup memory taken by VAO and VBOs
	if (vao != GL_NONE) glDeleteVertexArrays(1, &vao);
	if (vboPositions != GL_NONE) glDeleteBuffers(1, &vboPositions);
	if (vboTriangles != GL_NONE) glDeleteBuffers(1, &vboTriangles);
	if (vboTexUVs != GL_NONE) glDeleteBuffers(1, &vboTexUVs);
	if (vboAlphas != GL_NONE) glDeleteBuffers(1, &vboAlphas);
	
	if (vboData.Triangles != nullptr)
	{
		delete[] vboData.Triangles;
		delete[] vboData.TextureUVs;
		delete[] vboData.Alphas;
	}

	if (particlesList.Positions != nullptr)
	{
		delete[] particlesList.Positions;
		delete[] particlesList.Velocities;
		delete[] particlesList.Size;
		delete[] particlesList.Alpha;
		delete[] particlesList.Ages;
		delete[] particlesList.Lifetimes;
	}
}

bool ParticleSystem::Init(vec3 positionMinimum, vec3 positionMaximum, vec3 velocityMinimum, vec3 velocityMaximum, 
	vec2 lifeTimeRangeMinMax, vec2 alphaRangeMinMax, vec2 sizeRangeMinMax, 
	unsigned int particlesToGenerate, unsigned int rate, const char* fileParticlePath, bool isUsingGeometryShader)
{
	isGeometryGPU = isUsingGeometryShader;

	// Initialize shader program, and load shaders - using geometry shader
	if (isGeometryGPU)
	{
		if (!progParticleSystem.Load("./Assets/Shaders/ParticlesGPU.vert", "./Assets/Shaders/ParticlesGPU.geom", "./Assets/Shaders/ParticlesGPU.frag"))
		{
			std::printf("Shader program failed to initialize!");
			return false;
		}
	}
	// Not using geometry shader
	else
	{
		if (!progParticleSystem.Load("./Assets/Shaders/ParticlesCPU.vert", "./Assets/Shaders/ParticlesCPU.frag"))
		{
			std::printf("Shader program failed to initialize!");
			return false;
		}
	}

	// Initialize particle texture
	if (!particleTexture.Load(fileParticlePath))
	{
		std::printf("Particle texture failed to load!");
		return false;
	}

	// Randomize seed for random number generation
	srand(static_cast<unsigned int>(time(NULL)));

	// Set values for particle properties
	positionRangeMinimum = positionMinimum;
	positionRangeMaximum = positionMaximum;
	velocityRangeMinimum = velocityMinimum;
	velocityRangeMaximum = velocityMaximum;
	lifetimeRange = lifeTimeRangeMinMax;
	alphaRange = alphaRangeMinMax;
	sizeRange = sizeRangeMinMax;
	maxParticles = particlesToGenerate;
	emitRate = rate;
	
	particlesList.Positions = new vec3[maxParticles];		// These are centers of the particles
	particlesList.Velocities = new vec3[maxParticles];		// Velocities
	particlesList.Alpha = new float[maxParticles];			// Alphas
	particlesList.Ages = new float[maxParticles];			// Ages
	particlesList.Lifetimes = new float[maxParticles];		// Lifetimes
	particlesList.Size = new float[maxParticles];			// Sizes

	// Generate and bind VAO
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// If using geometry shader to create additonal geometry
	if (isGeometryGPU)
	{
		// Generate attribute buffers
		glGenBuffers(1, &vboPositions);						// VBO (particle centers)
		glGenBuffers(1, &vboSizes);							// VBO (particle sizes)
		glGenBuffers(1, &vboAlphas);						// VBO (alphas)

		// Enable vertex attribute arrays
		glEnableVertexAttribArray(0);						// Enable data array pointer for centers
		glEnableVertexAttribArray(1);						// Enable data array pointer for sizes
		glEnableVertexAttribArray(2);						// Enable data array pointer for alphas

		int vboPositionsSize = maxParticles * sizeof(float)* 3;	// position (vec3) 3 floats
		int vboSizesSize = maxParticles * sizeof(float)* 1;		// size (float) is single float
		int vboAlphasSize = maxParticles * sizeof(float)* 1;	// alpha (float) is single float

		// Bind buffers and point to VBO attributes data
		glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
		glBufferData(GL_ARRAY_BUFFER, vboPositionsSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0u, 3, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, vboSizes);
		glBufferData(GL_ARRAY_BUFFER, vboSizesSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1u, 1, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
		glBufferData(GL_ARRAY_BUFFER, vboAlphasSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(2u, 1, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	}
	// Not using geometry shader
	else
	{
		// Initialize collections for each particle property (each particle will be represented by two triangles (thus 6 corners)
		geomObjSize = 6;

		// Initialize arrays needed to store CPU created data
		vboData.Triangles = new vec4[maxParticles * geomObjSize];
		vboData.TextureUVs = new vec2[maxParticles * geomObjSize];
		vboData.Alphas = new float[maxParticles * geomObjSize];

		// Generate attribute buffers
		glGenBuffers(1, &vboTriangles);							// VBO (corners data)
		glGenBuffers(1, &vboTexUVs);							// VBO (texture UVs)
		glGenBuffers(1, &vboAlphas);							// VBO (alphas)

		// Enable vertex attribute arrays
		glEnableVertexAttribArray(0);							// Enable data array pointer for corners
		glEnableVertexAttribArray(1);							// Enable data array pointer for UVs
		glEnableVertexAttribArray(2);							// Enable data array pointer for alphas

		int vboTrianglesSize = maxParticles * geomObjSize * sizeof(float)* 4;	// triangle corner (vec4) is 4 floats
		int vboTexUVsSize = maxParticles * geomObjSize * sizeof(float)* 2;		// texture UV (vec2) is 2 floats
		int vboAlphasSize = maxParticles * geomObjSize * sizeof(float)* 1;		// alpha (float) is single float

		// Bind buffers and point to VBO attributes data
		glBindBuffer(GL_ARRAY_BUFFER, vboTriangles);
		glBufferData(GL_ARRAY_BUFFER, vboTrianglesSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0u, 4, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, vboTexUVs);
		glBufferData(GL_ARRAY_BUFFER, vboTexUVsSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1u, 2, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
		glBufferData(GL_ARRAY_BUFFER, vboAlphasSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(2u, 1, GL_FLOAT, GL_FALSE, 0, (const void*)0);

		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	}
	return true;
}

void ParticleSystem::Update(float elapsed)
{
	int NumToSpawn = emitRate;

	// Create new particlesList
	while (particlesCreated < maxParticles && NumToSpawn > 0)
	{
		particlesList.Ages[particlesCreated] = 0.0f;
		particlesList.Lifetimes[particlesCreated] = GenerateRandomFloat(lifetimeRange.x, lifetimeRange.y);
		particlesList.Size[particlesCreated] = GenerateRandomFloat(sizeRange.x, sizeRange.y);
		particlesList.Alpha[particlesCreated] = GenerateRandomFloat(alphaRange.x, alphaRange.y);

		// Set the particle positions and send them in a random direction
		particlesList.Positions[particlesCreated] = 
			vec3
			(
				GenerateRandomFloat(positionRangeMinimum.x, positionRangeMaximum.x),	// Randomize position on X axis between min and max values
				GenerateRandomFloat(positionRangeMinimum.y, positionRangeMaximum.y),	// Randomize position on Y axis between min and max values 
				GenerateRandomFloat(positionRangeMinimum.z, positionRangeMaximum.z)		// Randomize position on Z axis between min and max values
			);

		particlesList.Velocities[particlesCreated] = 
			vec3
			(
				GenerateRandomFloat(velocityRangeMinimum.x, velocityRangeMaximum.x),	// Randomize velocity on X axis between min and max values
				GenerateRandomFloat(velocityRangeMinimum.y, velocityRangeMaximum.y),	// Randomize velocity on Y axis between min and max values
				GenerateRandomFloat(velocityRangeMinimum.z, velocityRangeMaximum.z)		// Randomize velocity on Z axis between min and max values
			);

		// For non GPU geometry
		if (!isGeometryGPU)
		{
			// Generate corners forming triangles for each particle center
			for (unsigned int i = 0; i < geomObjSize; i++)
			{
				vboData.Triangles[particlesCreated * geomObjSize + i] = GenerateParticleTriangleCorner(i, particlesList.Positions[particlesCreated], particlesList.Size[particlesCreated]);
				vboData.TextureUVs[particlesCreated * geomObjSize + i] = GenerateTextureCoordinates(i);
				vboData.Alphas[particlesCreated * geomObjSize + i] = particlesList.Alpha[particlesCreated];
			}
		}
		
		// Increment particle counters...
		particlesCreated++;
		NumToSpawn--;
	}

	/// Update existing particlesList ///
	for (unsigned i = 0; i < particlesCreated; i++)
	{
		particlesList.Ages[i] += elapsed;	// Update age by elapsed time
		
		// Eliminate particles when they reach maximum age
		if (particlesList.Ages[i] > particlesList.Lifetimes[i])
		{
			//remove the particle by replacing it with the one at the top of the stack
			particlesList.Alpha[i] = particlesList.Alpha[particlesCreated - 1];
			particlesList.Ages[i] = particlesList.Ages[particlesCreated - 1];
			particlesList.Lifetimes[i] = particlesList.Lifetimes[particlesCreated - 1];
			particlesList.Positions[i] = particlesList.Positions[particlesCreated - 1];
			particlesList.Size[i] = particlesList.Size[particlesCreated - 1];
			particlesList.Velocities[i] = particlesList.Velocities[particlesCreated - 1];
			particlesCreated--;
			continue;
		}

		particlesList.Positions[i] += particlesList.Velocities[i] * elapsed;

		float interp = particlesList.Ages[i] / particlesList.Lifetimes[i];

		particlesList.Alpha[i] = LERP(alphaRange.x, alphaRange.y, interp);
		particlesList.Size[i] = LERP(sizeRange.x, sizeRange.y, interp);

		// For non GPU geometry
		if (!isGeometryGPU)
		{
			// Update quads for each particle
			for (unsigned int j = 0; j < geomObjSize; j++)
			{
				vboData.Triangles[i * geomObjSize + j] = GenerateParticleTriangleCorner(j, particlesList.Positions[i], particlesList.Size[i]);
				vboData.TextureUVs[i * geomObjSize + j] = GenerateTextureCoordinates(j);
				vboData.Alphas[i * geomObjSize + j] = particlesList.Alpha[i];
			}
		}
	}

	// Set particle random color
	randomColorSeed = vec3(
		GenerateRandomFloat(0.4f, 0.6f), 
		GenerateRandomFloat(0.4f, 0.6f), 
		GenerateRandomFloat(0.4f, 0.6f)
	);

	//
	// Write any particle changes to VBO subdata buffers
	//
	// For GPU geometry
	if (isGeometryGPU)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3)* particlesCreated, &particlesList.Positions[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vboSizes);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)* particlesCreated, &particlesList.Size[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)* particlesCreated, &particlesList.Alpha[0]);

		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	}
	// For CPU geometry
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboTriangles);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec4)* particlesCreated * geomObjSize, &vboData.Triangles[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vboTexUVs);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec2)* particlesCreated * geomObjSize, &vboData.TextureUVs[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vboAlphas);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)* particlesCreated * geomObjSize, &vboData.Alphas[0]);

		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	}
}

void ParticleSystem::Draw()
{
	// Loop through all particles created and draw them
	if (particlesCreated != 0)
	{
		// Alpha blending
		glEnable(GL_BLEND);													// Enable alpha blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);					// Define blending function
		
		glDepthMask(GL_FALSE);												// Disable depth writing

		// Activate texture 0 and bind Texture object to it
		glActiveTexture(GL_TEXTURE0);
		particleTexture.Bind();

		// Bind shader program and send uniforms to shaders
		progParticleSystem.Bind();
		progParticleSystem.SendUniformMat4("uModel", modelMatrix.data, true);		// Set uniform: model matrix data
		progParticleSystem.SendUniformMat4("uView", viewMatrix.data, true);			// Set uniform: view matrix data
		progParticleSystem.SendUniformMat4("uProj", projectionMatrix.data, true);	// Set uniform: projection matrix data
		progParticleSystem.SendUniform("uTex", 0);									// Set uniform: texture
		progParticleSystem.SendUniform("uRandomColorSeed", randomColorSeed);		// Set uniform: random color seed

		// ======================================================
		// Drawing via VAO
		glBindVertexArray(vao);													// Bind VAO
		
		// --- Is geometry shader used? ---
		if (isGeometryGPU)
			glDrawArrays(GL_POINTS, 0, particlesCreated);						// Using points data as triangles geometry is generated on GPU

		// --- or is it CPU generated geometry ---
		else
			glDrawArrays(GL_TRIANGLES, 0, particlesCreated * geomObjSize);		// Draw  triangle strips using CPU (C++) generated data

		glBindVertexArray(GL_NONE);												// Unbind VAO

		// ======================================================

		glDepthMask(GL_TRUE);												// Enable depth writing
		
		glDisable(GL_BLEND);												// Disable blending

		particleTexture.Unbind();											// Unbind texture
	}
}

void ParticleSystem::SetMatrices(mat4 mProjection, mat4 mView, mat4 mModel)
{
	projectionMatrix = mProjection;
	viewMatrix = mView;
	modelMatrix = mModel;
}